#!/usr/bin/env ruby

# Copyirhgt (C) 2013 shiro <shiro@worldofcorecraft.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

class Course
    def initialize(status, config, output, irc, timer)
        @status  = status
        @config  = config
        @output  = output
        @irc     = irc
        @timer   = timer

        # Users pending to ask questions
        @users = Array.new

        @channel = ""

        @asker = ""
    end

    # !course ask -- Let the bot know you want to ask a question
    def ask(nick, user, host, from, msg, arguments, con)
        if (@channel.length == 0)
            return
        end

        if (@users.include?(nick))
            @irc.message(nick, "You're already in the queue! Position: " + (@users.index(nick)+1).to_s() + ".")
        else
            @users.push(nick)
            @irc.message(nick, "You have been added to the queue. Position: " + @users.length.to_s() + ".");
            @irc.message(nick, "If your question is answered before it's your turn, please cancel it with \"!course cancel\".")
        end
    end

    # !course cancel -- Cancels your current pending question
    def cancel(nick, user, host, from, msg, arguments, con)
        if (@channel.length == 0)
            return
        end

        if (@users.include?(nick))
            @users.delete(nick)
            @irc.message(nick, "You have been removed from the queue.")
        else
            @irc.message(nick, "You're not in the queue.")
        end
    end

    # !course qnext -- Gives voice to the person that's next in line to ask a question
    def qnext(nick, user, host, from, msg, arguments, con)
        if (@channel.length == 0)
            return
        end

        if (!@config.auth(host, con))
            return
        end

        if (@asker.length != 0)
            @irc.mode(@channel, "-v", @asker)
        end

        if (@users.length == 0)
            @irc.message(@channel, "Question queue is empty!")
            return
        end

        # TODO: Make sure user is actually still in the channel
        @asker = @users.first
        @irc.mode(@channel, "+v", @asker)

        @users.delete(@asker)

        @irc.message(@asker, "You have been given the ability to speak. Please ask your question in " + @channel + " now.")
    end

    # !course qend -- Cancel current question
    def qend(nick, user, host, from, msg, arguments, con)
        if (@channel.length == 0)
            return
        end

        if (!@config.auth(host, con))
            return
        end

        if (@asker.length != 0)
            @irc.mode(@channel, "-v", @asker)
        end
        @asker = ""
    end

    # !course qclear -- Removes all pending questions
    def qclear(nick, user, host, from, msg, arguments, con)
        @users.clear;
        if (@channel.length != 0)
            @irc.message(@channel, "All pending questions have been cleared. If you still had something to ask, please write \"!course ask\" again.");
        end
    end

    # !course start #channel -- The bot starts moderating this channel
    def start(nick, user, host, from, msg, arguments, con)
        if (@config.auth(host, con))
            if (@channel.length != 0)
                @irc.message(nick, "Bot is already running!")
                return
            end

            if (arguments.split[0] == nil)
                @irc.message(nick, "Need to specify channel name!")
                return
            end

            @channel = arguments.split[0]
            if (@channel[0] != '#')
                @channel = ""
                @irc.message(nick, "Invalid channel name!")
                return
            end

            @irc.join(@channel)
        else
            @irc.message(nick, "You don't have the permission to do that!")
        end
    end

    # !course stop -- Makes the channel stop course moderating its channel
    def stop(nick, user, host, from, msg, arguments, con)
        if (@config.auth(host, con))
            if (@channel.length == 0)
                @irc.message(nick, "Bot is not running!")
                return
            end

            @irc.part(@channel)
            @channel = ""
        else
            @irc.message(nick, "You don't have the permission to do that!")
        end
    end

    # Inform users that join how the bot system works
    def joined(nick, user, host, channel)
        if (channel != @channel)
            return
        end

        if (nick == @config.nick)
            return
        end

        @irc.message(nick, "Hi! " + @channel + " is moderated by me. You're unable to speak by default. " +
            "If you have a question please type \"!course ask\" to me in private chat and you'll be given " +
            "voice in " + @channel + " once it's your turn.")
    end

    # Remove users when they part or quit from the asking queue
    def parted(nick, user, host, channel)
        if (@users.include?(nick))
            @users.delete(nick)
        end
    end
    def quited(nick, user, host, message)
        if (@users.include?(nick))
            @users.delete(nick)
        end
    end
end

